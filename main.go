package main

import (
	"bufio"
	"fmt"
	"log"
	"net/url"
	"os"

	"github.com/gen2brain/beeep"
	"github.com/gorilla/websocket"
)

func main() {
	var name string
	scanner := bufio.NewScanner(os.Stdin)
	err := beeep.Beep(beeep.DefaultFreq, beeep.DefaultDuration)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Enter your name:")
	fmt.Scanf("%s", &name)

	u := url.URL{Scheme: "ws", Host: "127.0.0.1:8000", Path: "/ws/" + name}

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	log.Println("Connected to chat server")
	go func() {

		for {
			_, message, _ := c.ReadMessage()
			fmt.Printf("\r\033[K")
			fmt.Printf("\r\033[K")
			fmt.Println(string(message))
			if len(string(message)) >= len(name) {
				if string(message)[:len(name)] != name {
					err = beeep.Notify(name, "You have a new message message", "assets/alert.png")
					if err != nil {
						log.Fatal(err)
					}
				}
			}
			fmt.Printf("%s:", name)
		}
	}()

	fmt.Printf("%s:", name)
	for scanner.Scan() {
		fmt.Printf("\033[A")
		fmt.Printf("\r\033[K")
		c.WriteMessage(websocket.TextMessage, []byte(scanner.Text()))
		fmt.Printf("%s:", name[:len(name)-1])
	}
}
