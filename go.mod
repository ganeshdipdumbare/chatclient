module gitlab.com/ganeshdipdumbare/chatclient

go 1.12

require (
	github.com/gen2brain/beeep v0.0.0-20190603194150-07ff5e574111
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/gorilla/websocket v1.4.0
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	github.com/tadvi/systray v0.0.0-20190226123456-11a2b8fa57af // indirect
	golang.org/x/sys v0.0.0-20190712062909-fae7ac547cb7 // indirect
	gopkg.in/toast.v1 v1.0.0-20180812000517-0a84660828b2 // indirect
)
